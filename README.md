# Jetclean :robot:

Jetclean is a small project where I try to make a robot drive around and clean up Lego Duplo bricks.


![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen)


## Table of Contents

- [Background](#background)
- [Dependence](#dependence)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Background

This project was made as a finishing one week project for an Embedded development course I took.

My son has a lot of Lego, and being 16 months old, he does know how to make a mess but not how to clean it up. So when thinking about a project that *"solve or address challenges or issues faced by yourself or society."* a robot that could clean up after him seemed like the perfect idea. It also seemed to be small enough of a project given I also had to learn how to use the robot.  


## Dependence

The project are dependent on two different hardware. 

- The [Jetson Nano Developer Kit](https://developer.nvidia.com/embedded/jetson-nano-developer-kit)

- The [JETANK AI Kit Tracked Mobile Robot](https://www.waveshare.com/jetank-ai-kit.htm)

The JETANK needs to be assembled with the Jetson Nano, flashed and connected to the internet. Instructions can be found [here](https://www.waveshare.com/wiki/JETANK_AI_Kit#Assembly_Guides)

## Install

Once the JETANK is connected to the internet, navigate to `http://<jetank_ip_address>:8888`. 

Open the `Terminal` by clicking on the icon under `Other` or `File->New->Terminal`.

![jetank_lancher](/img/jetank_lancher.png)

Then clone this repository.

```
git clone https://gitlab.com/Christoff_r/jetank_toy_cleaner.git
```

Then navigate to the `jetank_toy_cleaner` folder ,either by using the explore in the left side or by cd'ing into the folder from the `Terminal`.

## Usage

There are two steps to using the program.

### Start

To use the robot, simply run all but the last cells one by one.

After running the second to last cell the robot will start cleaning up Duplo bricks!

### Stop

To stop the robot again run the last cell.

## Contributing

If you have any tips, suggestions or things like that feel free to send me a message :speech_balloon:, although I might me slow to respond :sweat_smile:

## License

`UNLICENSED`